#from tkinter import *
#from tkinter.messagebox import *
from tkinter.filedialog import *
import pathlib
import subprocess
import os
from media import Media



def ouvrefichier1():
    #instance de l'objet media
    media1 = Media("", "", "", 0, 0, 0)
    #navigateur
    filechemin = askopenfilename(title="Ouvrir un fichier")

    media1.fill_value(filechemin)
    #Remplit le champ
    CheminG.delete(0, END)
    CheminG.insert(0, filechemin)
    # Affichage des metadonnées sur la page
    Label(info_fichier, text= media1.get_nom()+media1.get_extension(), font=("trebuchet", 12), bg='#505050', fg='white').grid(row=4, column=0, sticky='ew')
    Label(info_fichier, text='résolution : ' + str(media1.get_width()) + 'x' + str(media1.get_height()), font=("trebuchet", 12), bg='#505050',
      fg='white').grid(row=5, column=0, sticky='ew')
    Label(info_fichier, text='fréquence d\'image : ' + str(media1.get_framerate()) + ' fps', font=("trebuchet", 12), bg='#505050',
      fg='white').grid(row=6, column=0, sticky='ew')
    #Label(info_fichier, text='décimation : ' + str(decimation1), font=("trebuchet", 12), bg='#878787', fg='white').grid(row=7, column=0, sticky='ew')
    texte1.delete(0, END)
    texte1.insert(0, media1.get_nom())

    #Prerempli le nom de sorti du fichier
    if CheminG.get() != '' and CheminD.get() != '':
        outputtexte = os.path.dirname(CheminG.get()) + '/Split_' + texte1.get() + '_' + texte2.get() + '.mp4'
        output.delete(0, END)
        output.insert(0, outputtexte)

def ouvrefichier2():
    media2 = Media("", "", "", 0, 0, 0)
    filechemin = askopenfilename(title="Ouvrir un fichier")

    media2.fill_value(filechemin)
    #Remplit le champ
    CheminD.delete(0, END)
    CheminD.insert(0, filechemin)

    # Affichage des metadonnées sur la page
    Label(info_fichier, text= media2.get_nom()+media2.get_extension(), font=("trebuchet", 12), bg='#505050', fg='white').grid(row=4, column=1, sticky='ew')
    Label(info_fichier, text='résolution : ' + str(media2.get_width()) + ' x ' + str(media2.get_height()), font=("trebuchet", 12), bg='#505050',
      fg='white').grid(row=5, column=1, sticky='ew')
    Label(info_fichier, text='fréquence d\'image : ' + str(media2.get_framerate()) + ' fps', font=("trebuchet", 12), bg='#505050',
      fg='white').grid(row=6, column=1, sticky='ew')
    #Label(info_fichier, text='décimation : ' + str(decimation1), font=("trebuchet", 12), bg='#878787', fg='white').grid(row=7, column=0, sticky='ew')
    texte2.delete(0, END)
    texte2.insert(0, media2.get_nom())

    #Prerempli le nom de sorti du fichier
    if CheminG.get() != '' and CheminD.get() != '':
        outputtexte = os.path.dirname(CheminG.get()) + '/Split_' + texte1.get() + '_' + texte2.get() + '.mp4'
        output.delete(0, END)
        output.insert(0, outputtexte)


# def browse1():
#     # Permet à l'utilisateur d'ouvrir un fichier
#     # Déclaration des variables globales
#     global filepath1
#     global filename1
#     global filename1short
#     global chemin
#     global width1
#     global height1
#     global framerate1
#
#     filepath1 = askopenfilename(title="Ouvrir un fichier")
#     filename1 = os.path.basename(filepath1)
#     chemin = os.path.dirname(filepath1)
#
#
#     filename1short = os.path.splitext(filename1)[0]
#     #remplit les champs
#     if (filepath1 != ''):
#         # ffprobe génère un fichier .json contenant les métadonnées du fichier vidéo
#         subprocess.call(
#             [ffprobe_path, '-show_format', '-show_streams', '-print_format', 'json', '-i', filepath1, '>', 'info.json'],
#             shell=True)
#
#         # Ouverture du fichier .json
#         with open('info.json', 'r') as jsonfile:
#             result = json.load(jsonfile)
#         # On va chercher les metadonnées dont on a besoin (definition, fps, bitrate) et on les range dans des variables locales
#         if (result['streams'][0]['codec_type'] == 'video'):
#             width1 = result['streams'][0]['width']
#             height1 = result['streams'][0]['height']
#             framerate1 = result['streams'][0]['r_frame_rate']
#             decimation1=result['streams'][0]['pix_fmt']
#             if '/' in framerate1:
#                 framerate1 = round(
#                     int(framerate1[0:framerate1.find('/')]) / int(framerate1[framerate1.find('/') + 1:len(framerate1)]), 2)
#
#         # Suppression du fichier .json après l'avoir consulté
#             #os.remove("info.json")
#
#         # Affichage des metadonnées sur la page
#         Label(info_fichier, text=filename1, font=("trebuchet", 12), bg='#878787', fg='white').grid(row=4, column=0, sticky='ew')
#         Label(info_fichier, text='résolution : ' + str(width1) + 'x' + str(height1), font=("trebuchet", 12), bg='#878787', fg='white').grid(row=5, column=0, sticky='ew')
#         Label(info_fichier, text='fréquence d\'image : ' + str(framerate1) + ' fps', font=("trebuchet", 12), bg='#878787', fg='white').grid(row=6, column=0, sticky='ew')
#         Label(info_fichier, text='décimation : ' + str(decimation1), font=("trebuchet", 12), bg='#878787', fg='white').grid(row=7, column=0, sticky='ew')
#
#         texte1.delete(0, END)
#         texte1.insert(0, filename1short)
#
#         if filename1short !='' and filename2short !='':
#             outputtexte=chemin+'/Split_'+filename1short+'_'+filename2short+'.mp4'
#             output.delete(0, END)
#             output.insert(0, outputtexte)
#
# def browse2():
#     # Permet à l'utilisateur d'ouvrir un fichier
#     # Déclaration des variables globales
#     global filepath2
#     global filename2
#     global filename2short
#     global width2
#     global height2
#     global framerate2
#     global decimation2
#
#     filepath2 = askopenfilename(title="Ouvrir un fichier")
#     filename2 = os.path.basename(filepath2)
#     filename2short = os.path.splitext(filename2)[0]
#     # remplit le champ
#
#     if (filepath2 != ''):
#         # ffprobe génère un fichier .json contenant les métadonnées du fichier vidéo
#         subprocess.call(
#             [ffprobe_path, '-show_format', '-show_streams', '-print_format', 'json', '-i', filepath2, '>', 'info.json'],
#             shell=True)
#
#         # Ouverture du fichier .json
#         with open('info.json', 'r') as jsonfile:
#             result = json.load(jsonfile)
#         # On va chercher les metadonnées dont on a besoin (definition, fps, bitrate) et on les range dans des variables locales
#         if (result['streams'][0]['codec_type'] == 'video'):
#             width2 = result['streams'][0]['width']
#             height2 = result['streams'][0]['height']
#             framerate2 = result['streams'][0]['r_frame_rate']
#             decimation2 = result['streams'][0]['pix_fmt']
#             if '/' in framerate2:
#                 framerate2 = round(
#                     int(framerate2[0:framerate2.find('/')]) / int(framerate2[framerate2.find('/') + 1:len(framerate2)]),
#                     2)
#
#             # Suppression du fichier .json après l'avoir consulté
#             os.remove("info.json")
#
#         # Affichage des metadonnées sur la page
#         Label(info_fichier, text=filename2, font=("trebuchet", 12), bg='#878787', fg='white').grid(row=4, column=1,
#                                                                                                sticky='ew')
#         Label(info_fichier, text='résolution : ' + str(width2) + 'x' + str(height2), font=("trebuchet", 12), bg='#878787',
#               fg='white').grid(row=5, column=1, sticky='ew')
#         Label(info_fichier, text='fréquence d\'image : ' + str(framerate2) + ' fps', font=("trebuchet", 12), bg='#878787',
#               fg='white').grid(row=6, column=1, sticky='ew')
#         Label(info_fichier, text='décimation : ' + str(decimation2), font=("trebuchet", 12), bg='#878787', fg='white').grid(
#             row=7, column=1, sticky='ew')
#
#         texte2.delete(0, END)
#         texte2.insert(0, filename2short)
#
#         # if filename1short !='' and filename2short !='':
#         #     outputtexte=chemin+'/Split_'+filename1short+'_'+filename2short+'.mp4'
#         #     output.delete(0, END)
#         #     output.insert(0, outputtexte)

def browse3():
# save as
    filepath3 = asksaveasfilename(title="Enregistrer sous",filetypes = (("mp4 files","*.mp4"),("all files","*.*")))
    extension = pathlib.Path(filepath3).suffix

    if extension != '.mp4':
        filepath3=filepath3+'.mp4'
    output.delete(0, END)
    output.insert(0, filepath3)


def entry_change ():
    #selection des boutons radio

    entry_selection =radio_contact.get()
    if entry_selection == 1:
      codec = "libx264"
    elif entry_selection == 2:
      codec = "libx265"
    else:
      print ("selectionner un codec")


def affiche_popup(message):
        # creer une variable avec une instance de fenetre
        popup = Tk()
        # personnaliser la fenetre
        popup.title("Message d'alerte")
        popup.config(background='#505050')

        # affiche texte
        texte = Label(popup, text=message, font=("trebuchet", 12), bg='#505050', fg='white', pady=5, padx=10)
        texte.pack()

        # ajouter boutton OK
        button_enc = Button(popup, text="OK", font=("georgia", 12), bg='white', fg='#333333', pady=5, command=popup.destroy)
        button_enc.pack()

        # afficher la fenetre
        popup.mainloop()


def encode ():
    #Test presence de fichiers
    valeurdebitv = int(debit.get())
    if CheminG.get() == "" or CheminD.get() == "":
        affiche_popup("veuillez donner 2 fichiers d'entrée")
        return

    #Test valeur de débit
    valeurdebitv = int(debit.get())

    if type(valeurdebitv) != 'int':
        if valeurdebitv > 499:

            debitv = str(debit.get()) + "K"
        else:
            debit.delete(0, END)
            debit.insert(0, 0)
            affiche_popup("indiquez un débit supérieur à 500")
            return
    else:
        return

    # instance de l'objet media gauche
    media1 = Media("", "", "", 0, 0, 0)
    media1.fill_value(CheminG.get())
    # instance de l'objet media droit
    media2 = Media("", "", "", 0, 0, 0)
    media2.fill_value(CheminD.get())

    width =str(max (media1.get_width(), media2.get_width()))
    height=str(max (media1.get_height(), media2.get_height()))
    framerate = str(max (media1.get_framerate(), media2.get_framerate()))
    texteG = texte1.get()
    texteD = texte2.get()
    sortie = output.get()
    entry_selection = radio_contact.get()
    if entry_selection == 1:
      codec = "libx264"
    elif entry_selection == 2:
      codec = "libx265"
    else:
      print("selectionner un codec")

    subprocess.call(
      [ffmpeg_path, '-y', '-i', CheminG.get(),
       '-i', CheminD.get(),
       '-shortest',
       '-filter_complex',
       '[1:0]scale='+width+':'+height+':force_original_aspect_ratio=decrease,crop='+width+'/2:'+height+':'+width+'/2:0,framerate='+framerate+'[right];[0:0]scale='+width+':'+height+':force_original_aspect_ratio=decrease,framerate='+framerate+'[Left];[Left][right]overlay=shortest=1:x=('+width+'/2),drawtext=fontsize=100:text='+texteG+':fontcolor=white:x=(w/4)-(tw/2):y=(th),drawtext=fontsize=100:text='+texteD+':fontcolor=white:x=w-(w/4)-(tw/2):y=(th)',
       '-c:v',
       codec,
       '-b:v',
       debitv,
       sortie ],
       shell=True)
    affiche_popup("Encodage Terminé !")

#creer une variable avec une instance de fenetre
window = Tk()

#définition des variables
local_path = os.path.dirname(os.path.abspath("Splitscreen.py"))
ffmpeg_path = local_path + r'\tools\ffmpeg.exe'


#personnaliser la fenetre
window.title("Splitscreen")
window.geometry("480x780")
window.resizable(False, False)
window.iconbitmap("Logo.ico")
window.config(background='#505050')

#creation image
largeur = 100
hauteur = 100
image = PhotoImage(file="logo.png")
canvas = Canvas(window, width=largeur, height=hauteur, bg='#505050', bd=0, highlightthickness=0)
canvas.create_image(largeur/2, hauteur/2, image=image)
canvas.grid(row=0, column=1, columnspan=2, sticky='n')

#creation de frame choix_fichier

choix_fichier = LabelFrame(window, text="Choix des fichiers", padx=10, pady=10, bg='#505050', fg='white')
choix_fichier.grid(row=1, column=0, columnspan=4, sticky='ew')
choix_fichier.columnconfigure(0, weight=1)
choix_fichier.columnconfigure(1, weight=1)
#ajouter boutton1
button1 = Button(choix_fichier, text="Fichier Gauche", font=("georgia", 12), bg='white', fg='#333333', command=ouvrefichier1)
button1.grid(row=2, column=0, sticky="n")
#creer un champ text a ajouter 1
CheminG = Entry(choix_fichier, font=("trebuchet", 12), bg='#878787', fg='white', width=50)
CheminG.grid(row=3, column=0, sticky='ew', pady=10)
#ajouter boutton2
button2 = Button(choix_fichier, text="Fichier Droit", font=("georgia", 12), bg='white', fg='#333333', command=ouvrefichier2, padx=10)
button2.grid(row=4, column=0, sticky="n")
#creer un champ text a ajouter 1
CheminD = Entry(choix_fichier, font=("trebuchet", 12), bg='#878787', fg='white', width=50)
CheminD.grid(row=5, column=0, sticky='ew', pady=10)


#creation de frame info_fichier

info_fichier = LabelFrame(window, text="Infos des médias", padx=10, pady=10, bg='#505050', fg='white')
info_fichier.grid(row=2, column=0, columnspan=4, sticky='ew')
info_fichier.columnconfigure(0, weight=1)
info_fichier.columnconfigure(1, weight=1)
# texte vide d'info des medias
Label(info_fichier, text='                                               ', font=("trebuchet", 12), bg='#505050', fg='white').grid(row=4, column=0, sticky='ew')
Label(info_fichier, text='', font=("trebuchet", 12), bg='#505050', fg='white').grid(row=5, column=0, sticky='ew')
Label(info_fichier, text='', font=("trebuchet", 12), bg='#505050', fg='white').grid(row=6, column=0, sticky='ew')
Label(info_fichier, text='', font=("trebuchet", 12), bg='#505050', fg='white').grid(row=7, column=0, sticky='ew')
Label(info_fichier, text='                                               ', font=("trebuchet", 12), bg='#505050', fg='white').grid(row=4, column=1, sticky='ew')

#creation de frame Legendes
legende = LabelFrame(window, text="Légendes à incruster", padx=10, pady=10, bg='#505050', fg='white')
legende.grid(row=3, column=0, columnspan=4, sticky='ew')
legende.columnconfigure(0, weight=1)
legende.columnconfigure(1, weight=1)
# texte
Label(legende, text='gauche', font=("georgia", 12), bg='#505050', fg='white').grid(row=1, column=0, sticky='ew', pady=1)
Label(legende, text='droite', font=("georgia", 12), bg='#505050', fg='white').grid(row=1, column=1, sticky='ew', pady=1)
#creer un champ text a ajouter 1
texte1 = Entry(legende, font=("trebuchet", 14), bg='#878787', fg='white', justify='center')
texte1.grid(row=9, column=0, sticky='ew')
#creer un champ text a ajouter 2
texte2 = Entry(legende, font=("trebuchet", 14), bg='#878787', fg='white', justify='center')
texte2.grid(row=9, column=1, sticky='ew')


#creation de frame paramètres encodage
encodage = LabelFrame(window, text="Paramètres d'encodage", padx=10, pady=10, bg='#505050', fg='white')
encodage.grid(row=4, column=0, columnspan=4, sticky='ew')
encodage.columnconfigure(0, weight=1)
encodage.columnconfigure(1, weight=1)
encodage.columnconfigure(2, weight=1)
encodage.columnconfigure(3, weight=1)

#bouton radio selection encodage
label_title = Label(encodage, text="Codec :", font=("georgia", 12), bg='#505050', fg='white')
label_title.grid(row=1, column=0, sticky="w")
radio_contact = IntVar()
radiocontact1 = Radiobutton(encodage, text='H264', variable=radio_contact, indicatoron=0, value=1, command=entry_change, padx=5, pady=5, bg='#878787')
radiocontact1.grid(row=1, column=1, sticky='n')
radiocontact1.select()

radiocontact2 = Radiobutton(encodage, text='H265', variable=radio_contact, indicatoron=0, value=2, command=entry_change, padx=5, pady=5, bg='#878787')
radiocontact2.grid(row=1, column=2, sticky='n')
#creer un champ débit
# texte
label_title = Label(encodage, text="Débit en Kbits/s :", font=("georgia", 12), bg='#505050', fg='white')
label_title.grid(row=2, column=0, sticky="w", pady=10)
nombre = window.register(lambda s: not s or s.isdigit())
debit = Entry(encodage, font=("trebuchet", 14), bg='#878787', fg='white', validate='key', validatecommand=(nombre, '%P'))
debit.insert(0, "0")
debit.grid(row=2, column=1, sticky='ew', columnspan=3)


#creation de frame encodage
encodage = LabelFrame(window, text="Encodage", padx=10, pady=10, bg='#505050', fg='white')
encodage.grid(row=5, column=0, columnspan=4, sticky='ew')
encodage.columnconfigure(0, weight=1)
encodage.columnconfigure(1, weight=1)
encodage.columnconfigure(2, weight=1)
encodage.columnconfigure(3, weight=1)
# creer un champ Output
output = Entry(encodage, font=("trebuchet", 12), bg='#878787', fg='white')
output.grid(row=4, column=0, sticky='ew', columnspan=3)
# ajouter boutton3
button_out = Button(encodage, text="Output", font=("georgia", 12), bg='white', fg='#333333', command=browse3)
button_out.grid(row=4, column=3, sticky="n", columnspan=1)



# ajouter boutton encode
button_enc = Button(window, text="Encode", font=("georgia", 16), bg='white', fg='#333333', command=encode)
button_enc.grid(row=6, column=1, columnspan=2, sticky='n', pady=20)


#afficher la fenetre
window.mainloop()
