Permet de faire un écran partagé (splitscreen) entre 2 vidéo

si les 2 vidéos sont différentes convertit les vidéo en prenant la plus grand taille d'image et la plus courte durée

Permet d'afficher des légendes à gauche et à droite pour décrire ce qu'on compare


Nécessite :
Splitscreen.exe
logo.ico
logo.png
/tools

Développé en python par Christophe NELSON en utilisant FFmpeg
