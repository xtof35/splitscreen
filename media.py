import subprocess
import json
import os

class Media:
        def __init__(self, chemin, nom, extension, width, height, framerate):
            self.chemin = chemin
            self.nom = nom
            self.extension = extension
            self.width = width
            self.height = height
            self.framerate = framerate

        def get_chemin(self):
            return self.chemin

        def get_nom(self):
            return self.nom

        def get_extension(self):
            return self.extension

        def get_width(self):
            return self.width

        def get_height(self):
            return self.height

        def get_framerate(self):
            return self.framerate

        def fill_value(self, filepath):
            self.chemin = os.path.dirname(filepath)
            filename = os.path.basename(filepath)
            self.nom = os.path.splitext(filename)[0]
            self.extension = os.path.splitext(filename)[1]
            # remplit les champs
            if filepath != '':
                # ffprobe génère un fichier .json contenant les métadonnées du fichier vidéo
                local_path = os.path.dirname(os.path.abspath("Splitscreen.py"))
                ffprobe_path = local_path + r'\tools\ffprobe.exe'
                subprocess.call(
                    [ffprobe_path, '-show_format', '-show_streams', '-print_format', 'json', '-i', filepath, '>',
                     'info.json'],
                    shell=True)
                # Ouverture du fichier .json
                with open('info.json', 'r') as jsonfile:
                    result = json.load(jsonfile)
                # On va chercher les metadonnées dont on a besoin (definition, fps, bitrate) et on les range dans des variables locales
                if (result['streams'][0]['codec_type'] == 'video'):
                    self.width = result['streams'][0]['width']
                    self.height = result['streams'][0]['height']
                    framerate1 = result['streams'][0]['r_frame_rate']
                    #decimation1 = result['streams'][0]['pix_fmt']
                    if '/' in framerate1:
                        framerate1 = round(
                            int(framerate1[0:framerate1.find('/')]) / int(
                                framerate1[framerate1.find('/') + 1:len(framerate1)]), 2)
                    self.framerate=framerate1
                # Suppression du fichier .json après l'avoir consulté
                os.remove("info.json")
